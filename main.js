/*
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

    1.1 В JS каждый обект имеет прототип, который и есть ссылкой на другой обект.
        Когда ми попробуем получить доступ к свойсву или методу обьекта
        которого нет в существуещем обьекте, JS автоматически проверит его прототип
        и пойдёт таким образом искать свойство или метод пока не найдёт его.

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

    2.2 Когда ми создаём класс-наследователь конструктор этого класса 
        может вызывать конструктор родительского класса с помощью ключевого слова super().
        Это даст возможность наследователю унаследовать свойства 
        и обозначить их значения, вызывая конструктор родительского класса 
        перед тем как начнёться выполнение своего собственного кода(условия).
*/

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(name) {
        this._name = name;
    }

    set age(age) {
        this._age = age;
    }

    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, langs) {
        super(name,age,salary);
        this._langs = langs;
    }
    
    get salary() {
        return this._salary * 3;
    }
    
    get langs() {
        return this._langs;
    }
}

const programmerFirst = new Programmer("Sergey", 25, 1000, ["JS", "Python", "C++"]);
const programmerSecond = new Programmer("Mackseem", 30, 5000, ["JAVA", "PHP", "C#"]);
const programmerThird = new Programmer("Bogdan", 27, 10000, ["JS", "NodeJS"])

console.log(programmerFirst);
console.log("Name", programmerFirst.name);
console.log("Age", programmerFirst.age);
console.log("Salary", programmerFirst.salary);
console.log("Languages", programmerFirst.langs);


console.log(programmerSecond);
console.log("Name", programmerSecond.name);
console.log("Age", programmerSecond.age);
console.log("Salary", programmerThird.salary);
console.log("Languages", programmerSecond.langs);


console.log(programmerThird);
console.log("Name", programmerThird.name);
console.log("Age", programmerThird.age);
console.log("Salary", programmerThird.salary);
console.log("Languages", programmerThird.langs);